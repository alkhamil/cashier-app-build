<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\CategoryController;
use App\Http\Controllers\API\CustomerController;
use App\Http\Controllers\API\DashboardController;
use App\Http\Controllers\API\ExpenseController;
use App\Http\Controllers\API\JournalController;
use App\Http\Controllers\API\PayableController;
use App\Http\Controllers\API\PrintController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\API\PurchaseOrderController;
use App\Http\Controllers\API\ReceivableController;
use App\Http\Controllers\API\ReportController;
use App\Http\Controllers\API\SalesOrderController;
use App\Http\Controllers\API\SettingController;
use App\Http\Controllers\API\SupplierController;
use App\Http\Controllers\API\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->group(function () {
    // Begin User
    Route::get('user', [UserController::class, 'index']);
    Route::get('user/view', [UserController::class, 'view']);
    Route::post('user/save', [UserController::class, 'save']);
    Route::post('user/update', [UserController::class, 'update']);
    Route::get('user/delete', [UserController::class, 'delete']);
    Route::get('user/restore', [UserController::class, 'restore']);
    // End User

    // Begin Supplier
    Route::get('supplier', [SupplierController::class, 'index']);
    Route::get('supplier/view', [SupplierController::class, 'view']);
    Route::post('supplier/save', [SupplierController::class, 'save']);
    Route::post('supplier/update', [SupplierController::class, 'update']);
    Route::get('supplier/delete', [SupplierController::class, 'delete']);
    Route::get('supplier/restore', [SupplierController::class, 'restore']);
    // End Supplier

    // Begin Customer
    Route::get('customer', [CustomerController::class, 'index']);
    Route::get('customer/view', [CustomerController::class, 'view']);
    Route::post('customer/save', [CustomerController::class, 'save']);
    Route::post('customer/update', [CustomerController::class, 'update']);
    Route::get('customer/delete', [CustomerController::class, 'delete']);
    Route::get('customer/restore', [CustomerController::class, 'restore']);
    // End Customer

    // Begin Category
    Route::get('category', [CategoryController::class, 'index']);
    Route::get('category/view', [CategoryController::class, 'view']);
    Route::post('category/save', [CategoryController::class, 'save']);
    Route::post('category/update', [CategoryController::class, 'update']);
    Route::get('category/delete', [CategoryController::class, 'delete']);
    Route::get('category/restore', [CategoryController::class, 'restore']);
    // End Category

    // Begin Product
    Route::get('product', [ProductController::class, 'index']);
    Route::get('product/view', [ProductController::class, 'view']);
    Route::post('product/save', [ProductController::class, 'save']);
    Route::post('product/update', [ProductController::class, 'update']);
    Route::get('product/delete', [ProductController::class, 'delete']);
    Route::get('product/restore', [ProductController::class, 'restore']);
    Route::get('product/list-category', [ProductController::class, 'listCategory']);
    // End Product

    // Begin Sales Order
    Route::get('sales-order', [SalesOrderController::class, 'index']);
    Route::get('sales-order/view', [SalesOrderController::class, 'view']);
    Route::post('sales-order/save', [SalesOrderController::class, 'save']);
    Route::post('sales-order/update', [SalesOrderController::class, 'update']);
    Route::get('sales-order/delete-item', [SalesOrderController::class, 'deleteItem']);
    Route::get('sales-order/list-customer', [SalesOrderController::class, 'listCustomer']);
    Route::get('sales-order/list-product', [SalesOrderController::class, 'listProduct']);
    Route::get('sales-order/print', [SalesOrderController::class, 'print']);
    // End Sales Order

    // Begin Purchase Order
    Route::get('purchase-order', [PurchaseOrderController::class, 'index']);
    Route::get('purchase-order/view', [PurchaseOrderController::class, 'view']);
    Route::post('purchase-order/save', [PurchaseOrderController::class, 'save']);
    Route::get('purchase-order/list-supplier', [PurchaseOrderController::class, 'listSupplier']);
    Route::get('purchase-order/list-product', [PurchaseOrderController::class, 'listProduct']);
    Route::get('purchase-order/print', [PurchaseOrderController::class, 'print']);

    // End Purchase Order

    // Begin Expense
    Route::get('expense', [ExpenseController::class, 'index']);
    Route::get('expense/view', [ExpenseController::class, 'view']);
    Route::post('expense/save', [ExpenseController::class, 'save']);
    // End Expense

    // Begin Receivable
    Route::get('receivable', [ReceivableController::class, 'index']);
    Route::get('receivable/view', [ReceivableController::class, 'view']);
    Route::post('receivable/update', [ReceivableController::class, 'update']);
    // End Receivable
    
    // Begin Payable
    Route::get('payable', [PayableController::class, 'index']);
    Route::get('payable/view', [PayableController::class, 'view']);
    Route::post('payable/update', [PayableController::class, 'update']);
    // End Payable

    // Begin Report
    Route::get('report/sales', [ReportController::class, 'reportSales']);
    Route::get('report/purchase', [ReportController::class, 'reportPurchase']);
    Route::get('report/product-sales', [ReportController::class, 'reportProductSales']);
    Route::get('report/product-stock', [ReportController::class, 'reportProductStock']);
    Route::get('report/journal', [ReportController::class, 'reportJournal']);
    Route::get('report/list-product', [ReportController::class, 'listProduct']);

    // End Report

    // Begin Setting
    Route::get('setting/view', [SettingController::class, 'view']);
    Route::post('setting/update', [SettingController::class, 'update']);
    Route::post('setting/test-print', [SettingController::class, 'testPrint']);
    // End Setting
    
    Route::get('dashboard/index', [DashboardController::class, 'index']);
    Route::post('auth/logout', [AuthController::class, 'logout']);
});

Route::post('auth/login', [AuthController::class, 'login']);
