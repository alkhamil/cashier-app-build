<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class InitialDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // init data user
        $users = [
            [
                "email" => "kasir@pos.com",
                "name" => "Kasir",
                "username" => "kasir",
                "password" => Hash::make("kasir"),
                "role" => "KASIR"
            ],
            [
                "email"=> "admin@pos.com",
                "name" => "Admin",
                "username" => "admin",
                "password" => Hash::make("admin"),
                "role"=> "ADMIN"
            ]
        ];

        foreach ($users as $user) {
            DB::table('users')->insert([
                "email" => $user["email"],
                "name" => $user["name"],
                "username" => $user["username"],
                "password" => $user["password"],
                "role" => $user["role"],
                "created_at" => date('Y-m-d H:i:s')
            ]);
        }

        // init data customer
        DB::table('customers')->insert([
            "name" => "@TUNAI",
            "created_at" => date('Y-m-d H:i:s')
        ]);


        // init data setting
        $settings = [
            [
                "key" => "OUTLET_NAME",
                "value" => "TOKO NENO",
            ],
            [
                "key" => "OUTLET_ADDRESS",
                "value" => "JL. Raya Sadeng",
            ],
            [
                "key" => "OUTLET_PHONE",
                "value" => "0856111111222",
            ],
            [
                "key" => "PRINTER_CONNECTION",
                "value" => "windows",
            ],
            [
                "key" => "PRINTER_HOST",
                "value" => "",
            ],
            [
                "key" => "PRINTER_PORT",
                "value" => "9100",
            ],
            [
                "key" => "PRINTER_PAPER",
                "value" => "32",
            ],
        ];

        foreach ($settings as $setting) {
            DB::table('settings')->insert([
                "key" => $setting["key"],
                "value" => $setting["value"]
            ]);
        }



    }
}
