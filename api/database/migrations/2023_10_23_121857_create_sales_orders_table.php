<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sales_order_number');
            $table->bigInteger('queue_number');
            $table->bigInteger('user_id');
            $table->bigInteger('customer_id');
            $table->enum('sales_mode', ["RETAIL", "GROSIR", "SEMI GROSIR"]);
            $table->bigInteger('total_price')->default(0);
            $table->bigInteger('cash_amount')->default(0);
            $table->bigInteger('change_amount')->default(0);
            $table->enum('payment_method', ["CASH", "TEMPO"])->default("CASH");
            $table->integer("tempo_duration")->nullable();
            $table->dateTime("due_date")->nullable();
            $table->enum('status', ["DRAFT", "PAID", "UNPAID"])->default("DRAFT");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_orders');
    }
}
