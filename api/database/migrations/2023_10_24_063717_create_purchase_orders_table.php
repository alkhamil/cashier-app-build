<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('purchase_order_number');
            $table->bigInteger('user_id');
            $table->bigInteger('supplier_id');
            $table->bigInteger('total_price')->default(0);
            $table->bigInteger('cash_amount')->default(0);
            $table->bigInteger('change_amount')->default(0);
            $table->enum('payment_method', ["CASH", "TEMPO"])->default("CASH");
            $table->integer("tempo_duration")->nullable();
            $table->dateTime("due_date")->nullable();
            $table->enum('status', ["PAID", "UNPAID"])->default("PAID");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
