<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('category_id');
            $table->string('name');
            $table->string('barcode')->unique();
            $table->string('satuan');
            $table->bigInteger('base_price')->default(0);
            $table->bigInteger('retail_price')->default(0);
            $table->bigInteger('grosir_price')->default(0);
            $table->bigInteger('semi_grosir_price')->default(0);
            $table->bigInteger('stock')->default(0);
            $table->enum('is_active', [0, 1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
