<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'settings'; // Specify the table name
    protected $primaryKey = 'key'; // Specify the primary key column
    public $incrementing = false; // Indicate that the primary key is not auto-incrementing
    protected $fillable = ['key', 'value']; // Specify the columns that can be mass-assigned
}
