<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Receivable extends Model
{
    use HasFactory;

    public function sales_order()
    {
        return $this->belongsTo(SalesOrder::class);
    }
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
