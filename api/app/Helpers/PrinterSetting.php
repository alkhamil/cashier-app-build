<?php

namespace App\Helpers;

use Exception;
use Mike42\Escpos\Printer;
use Mike42\Escpos\CapabilityProfile;
use Mike42\Escpos\PrintConnectors\CupsPrintConnector;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;

class PrinterSetting
{
  private $printer;
  private $settings;

  public function __construct(array $settings)
  {
    $this->printer = null;
    $this->settings = $settings;
  }

  public function init()
  {
    switch (strtolower($this->settings["PRINTER_CONNECTION"])) {
      case 'cups':
        $connector = new CupsPrintConnector($this->settings["PRINTER_HOST"]);
        break;
      case 'windows':
        $connector = new WindowsPrintConnector($this->settings["PRINTER_HOST"]);
        break;
      case 'bluetooth':
        $connector = new WindowsPrintConnector($this->settings["PRINTER_HOST"]);
        break;
      case 'network':
        $connector = new NetworkPrintConnector($this->settings["PRINTER_HOST"], $this->settings["PRINTER_PORT"]);
        break;
      default:
        $connector = new FilePrintConnector("php://stdout");
        break;
    }

    if ($connector) {
      // Load simple printer profile
      $profile = CapabilityProfile::load("default");
      // Connect to printer
      $this->printer = new Printer($connector, $profile);
    } else {
      throw new Exception('Invalid printer connector type. Accepted values are: cups');
    }
  }

  public function text($text)
  {
    if ($this->printer) {
      $this->printer->text($text);
    }
  }

  public function close()
  {
    if ($this->printer) {
      $this->printer->close();
    }
  }

  public function feed($feed = null)
  {
    if ($this->printer) {
      $this->printer->feed($feed);
    }
  }
  public function setJustification($alignment)
  {
    if ($this->printer) {
      $this->printer->setJustification($alignment);
    }
  }

  public function cut()
  {
    if ($this->printer) {
      $this->printer->cut();
    }
  }

  public function printDashedLine($paperLength = 32)
  {
    $line = '';

    for ($i = 0; $i < $paperLength; $i++) {
      $line .= '-';
    }

    if ($this->printer) {
      $this->printer->text($line);
    }
  }

}