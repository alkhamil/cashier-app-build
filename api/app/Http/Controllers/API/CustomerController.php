<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        try {

            $customers = Customer::query();

            if (!is_null($request->name)) {
                $customers->where('name', 'like', '%' . $request->name . '%');
            }

            if (!is_null($request->address)) {
                $customers->where('address', 'like', '%' . $request->address . '%');
            }

            if (!is_null($request->phone)) {
                $customers->where('phone', 'like', '%' . $request->phone . '%');
            }

            if (!is_null($request->is_active)) {
                $customers->where('is_active', '=', $request->is_active);
            }

            if ($request->sort) {
                $order_type = 'asc';
                $order_column = $request->sort;
                if (str_contains($request->sort, '-')) {
                    $order_type = 'desc';
                    $order_column = substr($request->sort, 1);
                }

                $customers->orderBy($order_column, $order_type);
            }

            $result = $customers->latest('id')->paginate($request->per_page);

            return response()->json([
                'data' => $result,
                'message' => 'Successfuly Fetching'
            ], 200);
        } catch (Exception $error) {
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function view(Request $request)
    {
        try {
            $customer = Customer::firstWhere('id', $request->id);

            if (!$customer) {
                throw new Exception("Data not found!", 400);
            }

            return response()->json([
                'data' => $customer,
                'message' => 'Successfuly Fetching'
            ], 200);
        } catch (Exception $error) {
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function save(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);


            if ($validator->fails()) {
                throw new Exception($validator->errors(), 400);
            }

            $customer = new Customer();
            $customer->name = $request->name;
            $customer->address = $request->address;
            $customer->phone = $request->phone;
            $customer->created_at = date('Y-m-d H:i:s');

            if (!$customer->save()) {
                throw new Exception('Failed transaction DB!', 500);
            }

            DB::commit();
            return response()->json([
                'data' => $customer,
                'message' => 'Successfuly Created!'
            ], 201);
        } catch (Exception $error) {
            DB::rollBack();
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);

            if ($validator->fails()) {
                throw new Exception($validator->errors(), 400);
            }

            $customer = Customer::firstWhere('id', $request->id);

            if (!$customer) {
                throw new Exception("Data not found!", 400);
            }

            $customer->name = $request->name;
            $customer->address = $request->address;
            $customer->phone = $request->phone;
            $customer->updated_at = date('Y-m-d H:i:s');

            if (!$customer->save()) {
                throw new Exception('Failed transaction DB!', 500);
            }

            DB::commit();
            return response()->json([
                'data' => $customer,
                'message' => 'Successfuly Updated!'
            ], 200);
        } catch (Exception $error) {
            DB::rollBack();
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function delete(Request $request)
    {
        DB::beginTransaction();
        try {

            $customer = Customer::firstWhere('id', $request->id);
            
            if (!$customer) {
                throw new Exception("Data not found!", 400);
            }
            
            $customer->is_active = "0";
            if (!$customer->save()) {
                throw new Exception('Failed transaction DB!', 500);
            }

            DB::commit();
            return response()->json([
                'data' => $customer,
                'message' => 'Successfuly Deleted!'
            ], 200);
        } catch (Exception $error) {
            DB::rollBack();
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function restore(Request $request)
    {
        DB::beginTransaction();
        try {

            $customer = Customer::firstWhere('id', $request->id);
            
            if (!$customer) {
                throw new Exception("Data not found!", 400);
            }
            
            $customer->is_active = "1";
            if (!$customer->save()) {
                throw new Exception('Failed transaction DB!', 500);
            }

            DB::commit();
            return response()->json([
                'data' => $customer,
                'message' => 'Successfuly Restored!'
            ], 200);
        } catch (Exception $error) {
            DB::rollBack();
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }
}
