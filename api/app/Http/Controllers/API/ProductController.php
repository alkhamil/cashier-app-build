<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\ProductStock;
use App\Models\Unit;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
  public function index(Request $request)
  {
    try {

      $products = Product::query()->with(['category']);

      if (!is_null($request->name)) {
        $products->where('name', 'like', '%' . $request->name . '%');
      }

      if (!is_null($request->barcode)) {
        $products->where('barcode', 'like', '%' . $request->barcode . '%');
      }

      if (!is_null($request->catagory_name)) {
        $products->whereHas('category', function ($query) use ($request) {
          return $query->where('name', 'like', '%' . $request->catagory_name . '%');
        });
      }

      if (!is_null($request->category_id)) {
        $products->where('category_id', '=', $request->category_id);
      }


      if (!is_null($request->is_active)) {
        $products->where('is_active', '=', $request->is_active);
      }

      if ($request->sort) {
        $order_type = 'asc';
        $order_column = $request->sort;
        if (str_contains($request->sort, '-')) {
          $order_type = 'desc';
          $order_column = substr($request->sort, 1);
        }

        $products->orderBy($order_column, $order_type);
      }

      $result = $products->latest('id')->paginate($request->per_page);

      return response()->json([
        'data' => $result,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function view(Request $request)
  {
    try {
      $product = Product::firstWhere('id', $request->id);

      if (!$product) {
        throw new Exception("Data not found!", 400);
      }

      return response()->json([
        'data' => $product,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }


  private function saveProductStock($item)
  {
    $product_stock = new ProductStock();

    $product_stock->product_id = $item["product_id"];
    $product_stock->ref_number = $item["ref_number"];
    $product_stock->user_id = Auth::id();
    $product_stock->description = $item["description"];
    $product_stock->stock_in = $item["stock_in"];
    $product_stock->stock_out = $item["stock_out"];

    $latestProductStock = DB::table('product_stocks')
      ->where('product_id', $product_stock->product_id)
      ->latest('id')
      ->first();

    if ($latestProductStock) {
      $product_stock->stock_balance = $latestProductStock->stock_balance + $product_stock->stock_in;
    } else {
      $product_stock->stock_balance = 0 + $product_stock->stock_in;
    }

    if (!$product_stock->save()) {
      throw new Exception('Failed transaction DB!', 500);
    }
  }
  private function updateProductStock($item)
  {
    $product_stock = new ProductStock();

    $product_stock->product_id = $item["product_id"];
    $product_stock->ref_number = $item["ref_number"];
    $product_stock->user_id = Auth::id();
    $product_stock->description = $item["description"];
    $product_stock->stock_in = $item["stock_in"];
    $product_stock->stock_out = $item["stock_out"];

    $product_stock->stock_balance = $product_stock->stock_in;

    if (!$product_stock->save()) {
      throw new Exception('Failed transaction DB!', 500);
    }
  }

  public function save(Request $request)
  {
    DB::beginTransaction();
    try {
      $validator = Validator::make($request->all(), [
        'category_id' => 'required|exists:categories,id',
        'name' => 'required',
        'barcode' => 'required',
        'satuan' => 'required',
        'base_price' => 'required',
        'retail_price' => 'required',
        'grosir_price' => 'required',
        'semi_grosir_price' => 'required',
        'stock' => 'required',
      ]);


      if ($validator->fails()) {
        throw new Exception($validator->errors(), 400);
      }

      $product = new Product();
      $product->category_id = $request->category_id;
      $product->name = $request->name;
      $product->barcode = $request->barcode;
      $product->satuan = $request->satuan;
      $product->base_price = $request->base_price;
      $product->retail_price = $request->retail_price;
      $product->grosir_price = $request->grosir_price;
      $product->semi_grosir_price = $request->semi_grosir_price;
      $product->stock = $request->stock;
      $product->created_at = date('Y-m-d H:i:s');

      if (!$product->save()) {
        throw new Exception('Failed transaction DB!', 500);
      }

      $this->saveProductStock([
        'product_id' => $product->id,
        'ref_number'=> "MASTERDATA",
        'description'=> "Penambahan Produk Baru",
        'stock_in' => $product->stock,
        'stock_out'=> 0,
      ]);

      DB::commit();
      return response()->json([
        'data' => $product,
        'message' => 'Successfuly Created!'
      ], 201);
    } catch (Exception $error) {
      DB::rollBack();
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function update(Request $request)
  {
    DB::beginTransaction();
    try {
      $validator = Validator::make($request->all(), [
        'category_id' => 'required|exists:categories,id',
        'name' => 'required',
        'satuan' => 'required',
        'base_price' => 'required',
        'retail_price' => 'required',
        'grosir_price' => 'required',
        'semi_grosir_price' => 'required',
        'stock' => 'required',
      ]);

      if ($validator->fails()) {
        throw new Exception($validator->errors(), 400);
      }

      $product = Product::firstWhere('id', $request->id);

      if (!$product) {
        throw new Exception("Data not found!", 400);
      }

      $product->category_id = $request->category_id;
      $product->name = $request->name;
      $product->satuan = $request->satuan;
      $product->base_price = $request->base_price;
      $product->retail_price = $request->retail_price;
      $product->grosir_price = $request->grosir_price;
      $product->semi_grosir_price = $request->semi_grosir_price;
      $product->stock = $request->stock;
      $product->updated_at = date('Y-m-d H:i:s');

      if (!$product->save()) {
        throw new Exception('Failed transaction DB!', 500);
      }

      $this->updateProductStock([
        'product_id' => $product->id,
        'ref_number'=> "MASTERDATA",
        'description'=> "Perubahan Data Produk",
        'stock_in' => $product->stock,
        'stock_out'=> 0,
      ]);

      DB::commit();
      return response()->json([
        'data' => $product,
        'message' => 'Successfuly Updated!'
      ], 200);
    } catch (Exception $error) {
      DB::rollBack();
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function delete(Request $request)
  {
    DB::beginTransaction();
    try {

      $product = Product::firstWhere('id', $request->id);

      if (!$product) {
        throw new Exception("Data not found!", 400);
      }

      $product->is_active = "0";
      if (!$product->save()) {
        throw new Exception('Failed transaction DB!', 500);
      }

      DB::commit();
      return response()->json([
        'data' => $product,
        'message' => 'Successfuly Deleted!'
      ], 200);
    } catch (Exception $error) {
      DB::rollBack();
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function restore(Request $request)
  {
    DB::beginTransaction();
    try {

      $product = Product::firstWhere('id', $request->id);

      if (!$product) {
        throw new Exception("Data not found!", 400);
      }

      $product->is_active = "1";
      if (!$product->save()) {
        throw new Exception('Failed transaction DB!', 500);
      }

      DB::commit();
      return response()->json([
        'data' => $product,
        'message' => 'Successfuly Restored!'
      ], 200);
    } catch (Exception $error) {
      DB::rollBack();
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function listCategory()
  {
    try {
      $categories = Category::get();

      if (!$categories) {
        throw new Exception("Data not found!", 400);
      }

      return response()->json([
        'data' => $categories,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }
}
