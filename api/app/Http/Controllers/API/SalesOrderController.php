<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Journal;
use App\Models\Product;
use App\Models\ProductStock;
use App\Models\Receivable;
use App\Models\SalesOrder;
use App\Models\SalesOrderDetail;
use App\Models\Setting;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Mike42\Escpos\Printer;
use App\Helpers\PrinterSetting;


class SalesOrderController extends Controller
{
  public function index(Request $request)
  {
    try {

      $sales_orders = SalesOrder::query()->with([
        'user',
        'customer',
        'sales_order_details',
        'sales_order_details.product',
      ]);

      $sales_orders = $sales_orders->where(function ($query) use ($request) {
        $query->where('sales_order_number', 'like', '%' . $request->q . '%')
          ->orWhere('sales_mode', 'like', '%' . $request->q . '%')
          ->orWhereHas('user', function ($query) use ($request) {
            $query->where('name', 'like', '%' . $request->q . '%');
          })
          ->orWhereHas('customer', function ($query) use ($request) {
            $query->where('name', 'like', '%' . $request->q . '%');
          });
      });

      if (!is_null($request->created_at)) {
        $created_at = date('Y-m-d', strtotime($request->created_at));
        $sales_orders->whereDate('created_at', '=', $created_at);
      }

      if (!is_null($request->status)) {
        $sales_orders->where('status', '=', $request->status);
      }

      if ($request->sort) {
        $order_type = 'asc';
        $order_column = $request->sort;
        if (str_contains($request->sort, '-')) {
          $order_type = 'desc';
          $order_column = substr($request->sort, 1);
        }

        $sales_orders->orderBy($order_column, $order_type);
      }

      $result = $sales_orders->latest('created_at')->get();

      return response()->json([
        'data' => $result,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function view(Request $request)
  {
    try {
      $sales_order = SalesOrder::query()->with([
        'user',
        'customer',
        'sales_order_details',
        'sales_order_details.product',
      ])->where('id', '=', $request->id)->first();

      if (!$sales_order) {
        throw new Exception("Data not found!", 400);
      }

      return response()->json([
        'data' => $sales_order,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  private function saveJournal($item)
  {
    $journal = new Journal();
    $journal->journal_number = "JRN" . time();
    $journal->ref_number = $item["ref_number"];
    $journal->description = $item["description"];
    $journal->credit = $item["credit"];
    $journal->debit = $item["debit"];

    $latestJournal = DB::table('journals')
      ->latest('id')
      ->first();

    if ($latestJournal) {
      $journal->balance = $latestJournal->balance + $journal->credit;
    } else {
      $journal->balance = 0 + $journal->credit;
    }

    if (!$journal->save()) {
      throw new Exception('Failed transaction DB!', 500);
    }
  }

  private function saveProductStock(Product $product, $item)
  {
    $product_stock = new ProductStock();

    $product_stock->product_id = $item["product_id"];
    $product_stock->ref_number = $item["ref_number"];
    $product_stock->user_id = Auth::id();
    $product_stock->description = $item["description"];
    $product_stock->stock_in = $item["stock_in"];
    $product_stock->stock_out = $item["stock_out"];

    $latestProductStock = DB::table('product_stocks')
      ->where('product_id', $product_stock->product_id)
      ->latest('id')
      ->first();

    if ($latestProductStock) {
      $product_stock->stock_balance = $latestProductStock->stock_balance - $product_stock->stock_out;
    } else {
      $product_stock->stock_balance = 0 - $product_stock->stock_out;
    }

    if (!$product_stock->save()) {
      throw new Exception('Failed transaction DB!', 500);
    }

    $product->stock = $product_stock->stock_balance;

    if (!$product->save()) {
      throw new Exception('Failed transaction DB!', 500);
    }
  }
  private function saveReceivable($item)
  {
    $receivable = new Receivable();

    $receivable->receivable_number = "RCV" . time();
    $receivable->sales_order_id = $item["sales_order_id"];
    $receivable->customer_id = $item["customer_id"];
    $receivable->amount = $item["amount"];

    if (!$receivable->save()) {
      throw new Exception('Failed transaction DB!', 500);
    }
  }

  private function printReceipt(SalesOrder $sales_order)
  {
    try {
      $settings = Setting::pluck("value", "key")->toArray();
      $printer = new PrinterSetting($settings);
      $printer->init();
      if ($printer) {
        $printer->setJustification(Printer::JUSTIFY_LEFT);
        $printer->text("\n");
        $printer->text(
          str_pad('Toko', 10) .
          str_pad(':', 2) .
          $settings["OUTLET_NAME"] . "\n"
        );
        $printer->text(
          str_pad('Alamat', 10) .
          str_pad(':', 2) .
          $settings["OUTLET_ADDRESS"] . "\n"
        );
        $printer->text(
          str_pad('Telp', 10) .
          str_pad(':', 2) .
          $settings["OUTLET_PHONE"] . "\n"
        );
        $printer->text(
          str_pad('Faktur', 10) .
          str_pad(':', 2) .
          $sales_order->sales_order_number . "\n"
        );
        $createdAt = date('d/m/Y H:i:s', strtotime($sales_order->created_at));
        $printer->text(
          str_pad('Tanggal', 10) .
          str_pad(':', 2) .
          $createdAt . "\n"
        );
        $printer->text(
          str_pad('Pelanggan', 10) .
          str_pad(':', 2) .
          $sales_order->customer->name . "\n"
        );

        $printer->printDashedLine($settings["PRINTER_PAPER"]);

        $items = $sales_order->sales_order_details;

        // Loop through the items and print them
        foreach ($items as $item) {
          $printer->text($item->product->name . "\n");
          $price = number_format($item->price, 0, ",", ".");
          $total_price = number_format($item->total_price, 0, ",", ".");
          $printer->text(
            str_pad("x" . $item->qty . " @" . $price, $settings["PRINTER_PAPER"] - strlen($total_price)) .
            $total_price . "\n"
          );
        }

        $printer->printDashedLine($settings["PRINTER_PAPER"]);
        $printer->text("\n");

        // Calculate and print the total
        $grand_total = number_format($sales_order->total_price, 0, ",", ".");
        $cash_amount = number_format($sales_order->cash_amount, 0, ",", ".");
        $change_amount = number_format($sales_order->change_amount, 0, ",", ".");
        $printer->text(
          str_pad('Total Item', $settings["PRINTER_PAPER"] - strlen(count($items))) .
          count($items) . "\n"
        );
        $printer->text(
          str_pad('Total', $settings["PRINTER_PAPER"] - strlen($grand_total)) .
          $grand_total . "\n"
        );
        $printer->text(
          str_pad('Bayar', $settings["PRINTER_PAPER"] - strlen($cash_amount)) .
          $cash_amount . "\n"
        );
        $printer->text(
          str_pad('Sisa', $settings["PRINTER_PAPER"] - strlen($change_amount)) .
          $change_amount . "\n"
        );
        $printer->text(
          str_pad('Cara Bayar', $settings["PRINTER_PAPER"] - strlen($sales_order->payment_method)) .
          $sales_order->payment_method . "\n"
        );
        $status = "";
        switch ($sales_order->status) {
          case 'PAID':
            $status = "LUNAS";
            break;
          case 'UNPAID':
            $status = "BELUM LUNAS";
            break;
          case 'DRAFT':
            $status = "BELUM DIBAYAR";
            break;
          default:
            $status = "LUNAS";
            break;
        }
        $printer->text(
          str_pad('Status', $settings["PRINTER_PAPER"] - strlen($status)) .
          $status . "\n"
        );

        $printer->text("\n");

        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->text("Terima kasih.");
        $printer->text("\n");
        $printer->text("\n");

        $printer->cut();
        $printer->close();
      }

    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function print(Request $request)
  {
    try {
      $sales_order = SalesOrder::query()->with([
        'user',
        'customer',
        'sales_order_details',
        'sales_order_details.product',
      ])->where('id', '=', $request->id)->first();

      if (!$sales_order) {
        throw new Exception("Data not found!", 400);
      }

      $this->printReceipt($sales_order);

      return response()->json([
        'data' => $sales_order,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function save(Request $request)
  {
    DB::beginTransaction();
    try {
      $fieldValidator = [
        'customer_id' => 'required',
        'sales_mode' => 'required',
        'total_price' => 'required',
        'payment_method' => 'required',
        'sales_order_details' => 'required|array',
        'sales_order_details.*.product_id' => 'required|exists:products,id',
        'sales_order_details.*.qty' => 'required',
        'sales_order_details.*.price' => 'required',
        'sales_order_details.*.total_price' => 'required',
      ];

      if ($request->status !== "DRAFT") {
        $fieldValidator['cash_amount'] = 'required';
        $fieldValidator['change_amount'] = 'required';
      }

      $validator = Validator::make($request->all(), $fieldValidator);


      if ($validator->fails()) {
        throw new Exception($validator->errors(), 400);
      }

      $sales_order = new SalesOrder();
      $sales_order->user_id = Auth::id();
      $sales_order->customer_id = $request->customer_id;
      $sales_order->sales_mode = $request->sales_mode;
      $sales_order->sales_order_number = "SLO" . time();

      $current_date = date('Y-m-d');
      $sales_order_current_latest = SalesOrder::whereDate('created_at', $current_date)->latest('queue_number')->first();
      if ($sales_order_current_latest) {
        $sales_order->queue_number = $sales_order_current_latest->queue_number + 1;
      } else {
        $sales_order->queue_number = 1;
      }

      $sales_order->total_price = $request->total_price;
      if ($request->status !== "DRAFT") {
        $sales_order->cash_amount = $request->cash_amount;
        $sales_order->change_amount = $request->change_amount;
      }
      $sales_order->payment_method = $request->payment_method;

      if ($request->payment_method == "TEMPO") {
        $sales_order->tempo_duration = $request->tempo_duration;
        $sales_order->due_date = date("Y-m-d", strtotime($request->due_date));
      }

      $sales_order->created_at = date('Y-m-d H:i:s');

      if (!is_null($request->status)) {
        $sales_order->status = $request->status;
      } else {
        if ($sales_order->cash_amount >= $sales_order->total_price) {
          $sales_order->status = "PAID";
        } elseif ($sales_order->cash_amount < $sales_order->total_price && $sales_order->change_amount < 0) {
          $sales_order->status = "UNPAID";
        }
      }

      if (!$sales_order->save()) {
        throw new Exception('Failed transaction DB!', 500);
      }

      if (!empty($request->sales_order_details) && is_array($request->sales_order_details)) {
        foreach ($request->sales_order_details as $sales_order_detail) {
          $_sales_order_detail = new SalesOrderDetail();
          $_sales_order_detail->sales_order_id = $sales_order->id;
          $_sales_order_detail->product_id = $sales_order_detail["product_id"];
          $_sales_order_detail->qty = $sales_order_detail["qty"];
          $_sales_order_detail->price = $sales_order_detail["price"];
          $_sales_order_detail->total_price = $sales_order_detail["total_price"];

          $product = Product::firstWhere("id", $_sales_order_detail->product_id);

          if (!$product) {
            throw new Exception("Product not found!", 400);
          }

          if ($product->stock < $_sales_order_detail->qty) {
            throw new Exception('
              Stock product tidak cukup. sisa stock untuk product ' . $product->name . ' [' . $product->stock . ']',
              500
            );
          }

          if ($sales_order->status !== "DRAFT") {
            $this->saveProductStock($product, [
              'product_id' => $product->id,
              'ref_number' => $sales_order->sales_order_number,
              'description' => "Penjualan",
              'stock_in' => 0,
              'stock_out' => $_sales_order_detail->qty,
            ]);
          }

          if (!$_sales_order_detail->save()) {
            throw new Exception('Failed transaction DB!', 500);
          }

        }
      }

      if ($sales_order->status !== "DRAFT" && $sales_order->cash_amount > 0) {
        $this->saveJournal([
          "ref_number" => $sales_order->sales_order_number,
          "description" => "Penjualan",
          "credit" => $sales_order->cash_amount,
          "debit" => 0
        ]);
      }

      if ($sales_order->status === "UNPAID" && $sales_order->change_amount < 0) {
        $this->saveReceivable([
          "sales_order_id" => $sales_order->id,
          "customer_id" => $sales_order->customer_id,
          "amount" => abs($sales_order->change_amount),
        ]);
      }

      if ($sales_order->status !== "DRAFT") {
        $this->printReceipt($sales_order);
      }

      DB::commit();
      return response()->json([
        'data' => $sales_order,
        'message' => 'Successfuly Created!'
      ], 201);
    } catch (Exception $error) {
      DB::rollBack();
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }
  public function update(Request $request)
  {
    DB::beginTransaction();
    try {
      $fieldValidator = [
        'customer_id' => 'required',
        'sales_mode' => 'required',
        'total_price' => 'required',
        'payment_method' => 'required',
        'sales_order_details.*.product_id' => 'required|exists:products,id',
        'sales_order_details.*.qty' => 'required',
        'sales_order_details.*.price' => 'required',
        'sales_order_details.*.total_price' => 'required',
      ];

      if ($request->status !== "DRAFT") {
        $fieldValidator['cash_amount'] = 'required';
        $fieldValidator['change_amount'] = 'required';
      }

      $validator = Validator::make($request->all(), $fieldValidator);

      if ($validator->fails()) {
        throw new Exception($validator->errors(), 400);
      }

      $sales_order = SalesOrder::firstWhere('id', $request->id);

      if (!$sales_order) {
        throw new Exception("Data not found!", 400);
      }

      $sales_order->user_id = Auth::id();
      $sales_order->customer_id = $request->customer_id;
      $sales_order->sales_mode = $request->sales_mode;

      $sales_order->total_price = $request->total_price;
      if ($request->status !== "DRAFT") {
        $sales_order->cash_amount = $request->cash_amount;
        $sales_order->change_amount = $request->change_amount;
      }

      $sales_order->payment_method = $request->payment_method;

      if ($request->payment_method == "TEMPO") {
        $sales_order->tempo_duration = $request->tempo_duration;
        $sales_order->due_date = date("Y-m-d", strtotime($request->due_date));
      }

      $sales_order->updated_at = date('Y-m-d H:i:s');

      if (!is_null($request->status)) {
        $sales_order->status = $request->status;
      } else {
        if ($sales_order->cash_amount >= $sales_order->total_price) {
          $sales_order->status = "PAID";
        } elseif ($sales_order->cash_amount < $sales_order->total_price && $sales_order->change_amount < 0) {
          $sales_order->status = "UNPAID";
        }
      }

      if (!$sales_order->save()) {
        throw new Exception('Failed transaction DB!', 500);
      }

      if (!empty($request->sales_order_details) && is_array($request->sales_order_details)) {
        foreach ($request->sales_order_details as $sales_order_detail) {
          if (!empty($sales_order_detail["id"])) {
            $_sales_order_detail = SalesOrderDetail::firstWhere('id', $sales_order_detail["id"]);
          } else {
            $_sales_order_detail = new SalesOrderDetail();
          }
          $_sales_order_detail->sales_order_id = $sales_order->id;
          $_sales_order_detail->product_id = $sales_order_detail["product_id"];
          $_sales_order_detail->qty = $sales_order_detail["qty"];
          $_sales_order_detail->price = $sales_order_detail["price"];
          $_sales_order_detail->total_price = $sales_order_detail["total_price"];

          $product = Product::firstWhere("id", $_sales_order_detail->product_id);

          if (!$product) {
            throw new Exception("Product not found!", 400);
          }

          if ($product->stock < $_sales_order_detail->qty) {
            throw new Exception('
              Stock product tidak cukup. sisa stock untuk product ' . $product->name . ' [' . $product->stock . ']',
              500
            );
          }

          if ($sales_order->status !== "DRAFT") {
            $this->saveProductStock($product, [
              'product_id' => $product->id,
              'ref_number' => $sales_order->sales_order_number,
              'description' => "Penjualan",
              'stock_in' => 0,
              'stock_out' => $_sales_order_detail->qty,
            ]);
          }

          if (!$_sales_order_detail->save()) {
            throw new Exception('Failed transaction DB!', 500);
          }

        }
      }

      if ($sales_order->status === "PAID") {
        $this->saveJournal([
          "ref_number" => $sales_order->sales_order_number,
          "description" => "Penjualan",
          "credit" => $sales_order->total_price,
          "debit" => 0
        ]);
      }

      if ($sales_order->status === "UNPAID" && $sales_order->change_amount < 0) {
        $this->saveReceivable([
          "sales_order_id" => $sales_order->id,
          "customer_id" => $sales_order->customer_id,
          "amount" => abs($sales_order->change_amount),
        ]);
      }

      if ($sales_order->status !== "DRAFT") {
        $this->printReceipt($sales_order);
      }

      DB::commit();
      return response()->json([
        'data' => $sales_order,
        'message' => 'Successfuly Created!'
      ], 201);
    } catch (Exception $error) {
      DB::rollBack();
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function deleteItem(Request $request)
  {
    DB::beginTransaction();
    try {

      $sales_order_detail = SalesOrderDetail::firstWhere('id', $request->id);

      if (!$sales_order_detail) {
        throw new Exception("Data not found!", 400);
      }

      if (!$sales_order_detail->delete()) {
        throw new Exception('Failed transaction DB!', 500);
      }

      DB::commit();
      return response()->json([
        'data' => $sales_order_detail,
        'message' => 'Successfuly Deleted!'
      ], 200);
    } catch (Exception $error) {
      DB::rollBack();
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }
  public function listCustomer()
  {
    try {
      $customers = Customer::get();

      if (!$customers) {
        throw new Exception("Data not found!", 400);
      }

      return response()->json([
        'data' => $customers,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }
  public function listProduct(Request $request)
  {
    try {
      $products = Product::query();

      if (!is_null($request->name)) {
        $products->where('name', 'like', '%' . $request->name . '%');
      }

      $products->where('stock', '>', 0);

      $result = $products->paginate(10);

      return response()->json([
        'data' => $result,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

}
