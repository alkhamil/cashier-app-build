<?php

namespace App\Http\Controllers\API;

use App\Helpers\PrinterSetting;
use App\Http\Controllers\Controller;
use App\Models\Journal;
use App\Models\Payable;
use App\Models\Product;
use App\Models\ProductStock;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderDetail;
use App\Models\Setting;
use App\Models\Supplier;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Mike42\Escpos\Printer;

class PurchaseOrderController extends Controller
{
  public function index(Request $request)
  {
    try {

      $purchase_orders = PurchaseOrder::query()->with([
        'user',
        'supplier',
        'purchase_order_details',
        'purchase_order_details.product'
      ]);

      $purchase_orders = $purchase_orders->where(function ($query) use ($request) {
        $query->where('purchase_order_number', 'like', '%' . $request->q . '%')
          ->orWhere('payment_method', 'like', '%' . $request->q . '%')
          ->orWhereHas('user', function ($query) use ($request) {
            $query->where('name', 'like', '%' . $request->q . '%');
          })
          ->orWhereHas('supplier', function ($query) use ($request) {
            $query->where('name', 'like', '%' . $request->q . '%');
          });
      });

      if (!is_null($request->created_at)) {
        $created_at = date('Y-m-d', strtotime($request->created_at));
        $purchase_orders->whereDate('created_at', '=', $created_at);
      }

      if ($request->sort) {
        $order_type = 'asc';
        $order_column = $request->sort;
        if (str_contains($request->sort, '-')) {
          $order_type = 'desc';
          $order_column = substr($request->sort, 1);
        }

        $purchase_orders->orderBy($order_column, $order_type);
      }

      $result = $purchase_orders->latest('created_at')->get();

      return response()->json([
        'data' => $result,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function view(Request $request)
  {
    try {
      $purchase_order = PurchaseOrder::query()->with([
        'user',
        'supplier',
        'purchase_order_details',
        'purchase_order_details.product'
      ])->where('id', '=', $request->id)->first();

      if (!$purchase_order) {
        throw new Exception("Data not found!", 400);
      }

      return response()->json([
        'data' => $purchase_order,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  private function saveJournal($item)
  {
    $journal = new Journal();
    $journal->journal_number = "JRN" . time();
    $journal->ref_number = $item["ref_number"];
    $journal->description = $item["description"];
    $journal->credit = $item["credit"];
    $journal->debit = $item["debit"];

    $latestJournal = DB::table('journals')
      ->latest('id')
      ->first();

    if ($latestJournal) {
      $journal->balance = $latestJournal->balance - $journal->debit;
    } else {
      $journal->balance = 0 - $journal->debit;
    }

    if (!$journal->save()) {
      throw new Exception('Failed transaction DB!', 500);
    }
  }

  private function saveProductStock(Product $product, $item)
  {
    $product_stock = new ProductStock();

    $product_stock->product_id = $item["product_id"];
    $product_stock->ref_number = $item["ref_number"];
    $product_stock->user_id = Auth::id();
    $product_stock->description = $item["description"];
    $product_stock->stock_in = $item["stock_in"];
    $product_stock->stock_out = $item["stock_out"];

    $latestProductStock = DB::table('product_stocks')
      ->where('product_id', $product_stock->product_id)
      ->latest('id')
      ->first();

    if ($latestProductStock) {
      $product_stock->stock_balance = $latestProductStock->stock_balance + $product_stock->stock_in;
    } else {
      $product_stock->stock_balance = 0 + $product_stock->stock_in;
    }

    if (!$product_stock->save()) {
      throw new Exception('Failed transaction DB!', 500);
    }

    $product->stock = $product_stock->stock_balance;

    if (!$product->save()) {
      throw new Exception('Failed transaction DB!', 500);
    }
  }

  private function savePayable($item)
  {
    $payable = new Payable();

    $payable->payable_number = "PYB". time();
    $payable->purchase_order_id = $item["purchase_order_id"];
    $payable->supplier_id = $item["supplier_id"];
    $payable->amount = $item["amount"];

    if (!$payable->save()) {
      throw new Exception('Failed transaction DB!', 500);
    }
  }

  private function printReceipt(PurchaseOrder $purchase_order)
  {
    try {
      $settings = Setting::pluck("value", "key")->toArray();
      $printer = new PrinterSetting($settings);
      $printer->init();
      if ($printer) {
        $printer->setJustification(Printer::JUSTIFY_LEFT);
        $printer->text("\n");
        $printer->text(
          str_pad('Toko', 10) .
          str_pad(':', 2) .
          $settings["OUTLET_NAME"] . "\n"
        );
        $printer->text(
          str_pad('Alamat', 10) .
          str_pad(':', 2) .
          $settings["OUTLET_ADDRESS"] . "\n"
        );
        $printer->text(
          str_pad('Telp', 10) .
          str_pad(':', 2) .
          $settings["OUTLET_PHONE"] . "\n"
        );
        $printer->text(
          str_pad('Pembelian', 10) .
          str_pad(':', 2) .
          $purchase_order->purchase_order_number . "\n"
        );
        $createdAt = date('d/m/Y H:i:s', strtotime($purchase_order->created_at));
        $printer->text(
          str_pad('Tanggal', 10) .
          str_pad(':', 2) .
          $createdAt . "\n"
        );
        $printer->text(
          str_pad('Pemasok', 10) .
          str_pad(':', 2) .
          $purchase_order->supplier->name . "\n"
        );

        $printer->printDashedLine($settings["PRINTER_PAPER"]);

        $items = $purchase_order->purchase_order_details;

        // Loop through the items and print them
        foreach ($items as $item) {
          $printer->text($item->product->name . "\n");
          $price = number_format($item->price, 0, ",", ".");
          $total_price = number_format($item->total_price, 0, ",", ".");
          $printer->text(
            str_pad("x" . $item->qty . " @" . $price, $settings["PRINTER_PAPER"] - strlen($total_price)) .
            $total_price . "\n"
          );
        }

        $printer->printDashedLine($settings["PRINTER_PAPER"]);
        $printer->text("\n");

        // Calculate and print the total
        $grand_total = number_format($purchase_order->total_price, 0, ",", ".");
        $cash_amount = number_format($purchase_order->cash_amount, 0, ",", ".");
        $change_amount = number_format($purchase_order->change_amount, 0, ",", ".");
        $printer->text(
          str_pad('Total Item', $settings["PRINTER_PAPER"] - strlen(count($items))) .
          count($items) . "\n"
        );
        $printer->text(
          str_pad('Total', $settings["PRINTER_PAPER"] - strlen($grand_total)) .
          $grand_total . "\n"
        );
        $printer->text(
          str_pad('Bayar', $settings["PRINTER_PAPER"] - strlen($cash_amount)) .
          $cash_amount . "\n"
        );
        $printer->text(
          str_pad('Sisa', $settings["PRINTER_PAPER"] - strlen($change_amount)) .
          $change_amount . "\n"
        );
        $printer->text(
          str_pad('Cara Bayar', $settings["PRINTER_PAPER"] - strlen($purchase_order->payment_method)) .
          $purchase_order->payment_method . "\n"
        );
        $status = "";
        switch ($purchase_order->status) {
          case 'PAID':
            $status = "LUNAS";
            break;
          case 'UNPAID':
            $status = "BELUM LUNAS";
            break;
          case 'DRAFT':
            $status = "BELUM DIBAYAR";
            break;
          default:
            $status = "LUNAS";
            break;
        }
        $printer->text(
          str_pad('Status', $settings["PRINTER_PAPER"] - strlen($status)) .
          $status . "\n"
        );

        $printer->text("\n");

        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->text("Terima kasih.");
        $printer->text("\n");
        $printer->text("\n");

        $printer->cut();
        $printer->close();
      }

    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function print(Request $request) {
    try {
      $purchase_order = PurchaseOrder::query()->with([
        'user',
        'supplier',
        'purchase_order_details',
        'purchase_order_details.product',
      ])->where('id', '=', $request->id)->first();

      if (!$purchase_order) {
        throw new Exception("Data not found!", 400);
      }

      $this->printReceipt($purchase_order);

      return response()->json([
        'data' => $purchase_order,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function save(Request $request)
  {
    DB::beginTransaction();
    try {
      $validator = Validator::make($request->all(), [
        'supplier_id' => 'required',
        'payment_method' => 'required',
        'total_price' => 'required',
        'cash_amount' => 'required',
        'change_amount' => 'required',
        'purchase_order_details' => 'required|array',
        'purchase_order_details.*.product_id' => 'required|exists:products,id',
        'purchase_order_details.*.qty' => 'required',
        'purchase_order_details.*.price' => 'required',
        'purchase_order_details.*.total_price' => 'required',
      ]);


      if ($validator->fails()) {
        throw new Exception($validator->errors(), 400);
      }

      $purchase_order = new PurchaseOrder();
      $purchase_order->user_id = Auth::id();
      $purchase_order->supplier_id = $request->supplier_id;
      $purchase_order->payment_method = $request->payment_method;
      $purchase_order->purchase_order_number = "PRO" . time();
      $purchase_order->total_price = $request->total_price;
      $purchase_order->cash_amount = $request->cash_amount;
      $purchase_order->change_amount = $request->change_amount;

      if ($request->payment_method === "TEMPO") {
        $purchase_order->tempo_duration = $request->tempo_duration;
        $purchase_order->due_date = date("Y-m-d", strtotime($request->due_date));
      }

      $purchase_order->created_at = date('Y-m-d H:i:s');

      if ($purchase_order->cash_amount >= $purchase_order->total_price) {
        $purchase_order->status = "PAID";
      } elseif ($purchase_order->cash_amount < $purchase_order->total_price && $purchase_order->change_amount < 0) {
        $purchase_order->status = "UNPAID";
      }

      if (!$purchase_order->save()) {
        throw new Exception('Failed transaction DB!', 500);
      }

      if (!empty($request->purchase_order_details) && is_array($request->purchase_order_details)) {
        foreach ($request->purchase_order_details as $purchase_order_detail) {
          $_purchase_order_detail = new PurchaseOrderDetail();
          $_purchase_order_detail->purchase_order_id = $purchase_order->id;
          $_purchase_order_detail->product_id = $purchase_order_detail["product_id"];
          $_purchase_order_detail->qty = $purchase_order_detail["qty"];
          $_purchase_order_detail->price = $purchase_order_detail["price"];
          $_purchase_order_detail->total_price = $purchase_order_detail["total_price"];

          $product = Product::firstWhere("id", $_purchase_order_detail->product_id);

          if (!$product) {
            throw new Exception("Product not found!", 400);
          }

          $this->saveProductStock($product, [
            'product_id' => $product->id,
            'ref_number' => $purchase_order->purchase_order_number,
            'description' => "Pembelian",
            'stock_in' => $_purchase_order_detail->qty,
            'stock_out' => 0,
          ]);

          if (!$_purchase_order_detail->save()) {
            throw new Exception('Failed transaction DB!', 500);
          }

        }
      }

      $this->saveJournal([
        "ref_number" => $purchase_order->purchase_order_number,
        "description" => "Pembelian",
        "credit" => 0,
        "debit" => $purchase_order->cash_amount
      ]);

      if ($purchase_order->status === "UNPAID" && $purchase_order->change_amount < 0) {
        $this->savePayable([
          "purchase_order_id" => $purchase_order->id,
          "supplier_id" => $purchase_order->supplier_id,
          "amount" => abs($purchase_order->change_amount),
        ]);
      }

      $this->printReceipt($purchase_order);

      DB::commit();
      return response()->json([
        'data' => $purchase_order,
        'message' => 'Successfuly Created!'
      ], 201);
    } catch (Exception $error) {
      DB::rollBack();
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }
  public function listSupplier()
  {
    try {
      $suppliers = Supplier::get();

      if (!$suppliers) {
        throw new Exception("Data not found!", 400);
      }

      return response()->json([
        'data' => $suppliers,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function listProduct(Request $request)
  {
    try {
      $products = Product::query();

      if (!is_null($request->name)) {
        $products->where('name', 'like', '%' . $request->name . '%');
      }

      $result = $products->paginate(10);

      return response()->json([
        'data' => $result,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

}
