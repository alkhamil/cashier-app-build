<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Journal;
use App\Models\Receivable;
use App\Models\ReceivableDetail;
use App\Models\SalesOrder;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ReceivableController extends Controller
{
  public function index(Request $request)
  {
    try {

      $receivales = Receivable::query()->with([
        'sales_order',
        'customer',
      ]);

      $receivales = $receivales->where(function ($query) use ($request) {
        $query->where('receivable_number', 'like', '%' . $request->q . '%')
          ->orWhereHas('sales_order', function ($query) use ($request) {
            $query->where('sales_order_number', 'like', '%' . $request->q . '%');
          })
          ->orWhereHas('customer', function ($query) use ($request) {
            $query->where('name', 'like', '%' . $request->q . '%');
          });
      })->where('status', '=', 'UNPAID');

      if (!is_null($request->created_at)) {
        $created_at = date('Y-m-d', strtotime($request->created_at));
        $receivales->whereDate('created_at', '=', $created_at);
      }

      if ($request->sort) {
        $order_type = 'asc';
        $order_column = $request->sort;
        if (str_contains($request->sort, '-')) {
          $order_type = 'desc';
          $order_column = substr($request->sort, 1);
        }

        $receivales->orderBy($order_column, $order_type);
      }

      $result = $receivales->latest('created_at')->get();

      return response()->json([
        'data' => $result,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function view(Request $request)
  {
    try {
      $receivable = Receivable::with(['sales_order', 'customer'])->firstWhere('id', $request->id);

      if (!$receivable) {
        throw new Exception("Data not found!", 400);
      }

      return response()->json([
        'data' => $receivable,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  private function saveJournal($item)
  {
    $journal = new Journal();
    $journal->journal_number = "JRN" . time();
    $journal->ref_number = $item["ref_number"];
    $journal->description = $item["description"];
    $journal->credit = $item["credit"];
    $journal->debit = $item["debit"];

    $latestJournal = DB::table('journals')
      ->latest('id')
      ->first();

    if ($latestJournal) {
      $journal->balance = $latestJournal->balance + $journal->credit;
    } else {
      $journal->balance = 0 + $journal->credit;
    }

    if (!$journal->save()) {
      throw new Exception('Failed transaction DB!', 500);
    }
  }

  public function update(Request $request)
  {
    DB::beginTransaction();
    try {
      $validator = Validator::make($request->all(), [
        'sales_order_id' => 'required',
        'total_price' => 'required',
        'cash_amount' => 'required',
        'change_amount' => 'required',
        'amount' => 'required',
      ]);

      if ($validator->fails()) {
        throw new Exception($validator->errors(), 400);
      }

      $receivable = Receivable::firstWhere('id', $request->id);

      if (!$receivable) {
        throw new Exception("Data not found!", 400);
      }

      $receivable->amount = $request->change_amount;

      if ($receivable->amount > 0) {
        $receivable->status = "UNPAID";
      } else {
        $receivable->status = "PAID";
      }

      $receivable->updated_at = date('Y-m-d H:i:s');

      if (!$receivable->save()) {
        throw new Exception('Failed transaction DB!', 500);
      }

      $receivable_detail = new ReceivableDetail();
      $receivable_detail->receivable_id = $receivable->id;
      $receivable_detail->cash_amount = $request->amount;
      $receivable->created_at = date('Y-m-d H:i:s');


      if (!$receivable_detail->save()) {
        throw new Exception('Failed transaction DB!', 500);
      }

      $sales_order = SalesOrder::firstWhere('id', $receivable->sales_order_id);
      if (!$sales_order) {
        throw new Exception("Sales Order not found!", 400);
      }

      $sales_order->cash_amount = $request->cash_amount;
      $sales_order->change_amount = $request->change_amount;

      if ($sales_order->cash_amount >= $sales_order->total_price) {
        $sales_order->status = "PAID";
      } elseif ($sales_order->cash_amount < $sales_order->total_price && $sales_order->change_amount < 0) {
        $sales_order->status = "UNPAID";
      }

      if (!$sales_order->save()) {
        throw new Exception('Failed transaction DB!', 500);
      }

      // @note: will be impement soon!
      // if ($sales_order->status !== "DRAFT") {
      //   $this->saveJournal([
      //     "ref_number" => $sales_order->sales_order_number,
      //     "description" => "Penjualan",
      //     "credit" => $sales_order->cash_amount,
      //     "debit" => 0
      //   ]);
      // }

      DB::commit();
      return response()->json([
        'data' => $receivable,
        'message' => 'Successfuly Updated!'
      ], 200);
    } catch (Exception $error) {
      DB::rollBack();
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

}
