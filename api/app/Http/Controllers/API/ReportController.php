<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Journal;
use App\Models\Product;
use App\Models\ProductStock;
use App\Models\SalesOrder;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
  public function reportSales(Request $request)
  {
    try {

      $reportSales = DB::table('sales_orders as so')
        ->join('sales_order_details as sod', 'so.id', '=', 'sod.sales_order_id')
        ->join('products as p', 'sod.product_id', '=', 'p.id')
        ->join('customers as c', 'so.customer_id', '=', 'c.id')

        ->select([
          'so.created_at',
          'so.sales_order_number',
          'so.payment_method',
          'so.sales_mode',
          'c.name as customer_name',
          'so.status',
          DB::raw('COUNT(sod.id) as total_item'),
          DB::raw('SUM(p.base_price * sod.qty) as total_base_price'),
          DB::raw('SUM(sod.price * sod.qty) as total_sell_price'),
          DB::raw('SUM(sod.total_price - (p.base_price * sod.qty)) as total_profit'),
          DB::raw('(SUM(sod.total_price - (p.base_price * sod.qty))
          / SUM(p.base_price * sod.qty)) * 100 as total_profit_percentage'),
        ])
        ->groupBy('so.created_at', 'so.sales_order_number', 'so.payment_method', 'so.sales_mode', 'c.name', 'so.status')
        ->where(function ($query) use ($request) {
          $query->where('so.sales_order_number', 'like', '%' . $request->q . '%')
            ->orWhere('so.sales_mode', 'like', '%' . $request->q . '%')
            ->orWhere('so.payment_method', 'like', '%' . $request->q . '%')
            ->orWhere('c.name', 'like', '%' . $request->q . '%');
        })
        ->where('so.status', '=', 'PAID');

      if (!is_null($request->start_date) && !is_null($request->end_date)) {
        $reportSales = $reportSales->whereBetween('so.created_at', [$request->start_date, $request->end_date]);
      }

      $reportSales = $reportSales->get()
        ->map(function ($item) {
          $item->total_item = (float) $item->total_item;
          $item->total_base_price = (float) $item->total_base_price;
          $item->total_sell_price = (float) $item->total_sell_price;
          $item->total_profit = (float) $item->total_profit;
          $item->total_profit_percentage = (float) $item->total_profit_percentage;
          return $item;
        });


      return response()->json([
        'data' => $reportSales,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function reportPurchase(Request $request)
  {
    try {

      $reportPurchase = DB::table('purchase_orders as po')
        ->join('purchase_order_details as pod', 'po.id', '=', 'pod.purchase_order_id')
        ->join('products as p', 'pod.product_id', '=', 'p.id')
        ->join('suppliers as s', 'po.supplier_id', '=', 's.id')
        ->select([
          'po.created_at',
          'po.purchase_order_number',
          'po.payment_method',
          's.name as supplier_name',
          'po.status',
          DB::raw('COUNT(pod.id) as total_item'),
          DB::raw('SUM(p.base_price * pod.qty) as total_base_price'),
          DB::raw('SUM(pod.price * pod.qty) as total_sell_price'),
          DB::raw('SUM(pod.total_price - (p.base_price * pod.qty)) as total_profit'),
          DB::raw('(SUM(pod.total_price - (p.base_price * pod.qty))
          / SUM(p.base_price * pod.qty)) * 100 as total_profit_percentage'),
        ])
        ->groupBy('po.created_at', 'po.purchase_order_number', 'po.payment_method', 's.name', 'po.status')
        ->where(function ($query) use ($request) {
          $query->where('po.purchase_order_number', 'like', '%' . $request->q . '%')
            ->orWhere('po.payment_method', 'like', '%' . $request->q . '%')
            ->orWhere('s.name', 'like', '%' . $request->q . '%');
        })
        ->where('po.status', '=', 'PAID');

      if (!is_null($request->start_date) && !is_null($request->end_date)) {
        $reportPurchase = $reportPurchase->whereBetween('po.created_at', [$request->start_date, $request->end_date]);
      }

      $reportPurchase = $reportPurchase->get()
        ->map(function ($item) {
          $item->total_item = (float) $item->total_item;
          $item->total_base_price = (float) $item->total_base_price;
          $item->total_sell_price = (float) $item->total_sell_price;
          $item->total_profit = (float) $item->total_profit;
          $item->total_profit_percentage = (float) $item->total_profit_percentage;
          return $item;
        });


      return response()->json([
        'data' => $reportPurchase,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }
  public function reportProductSales(Request $request)
  {
    try {

      $reportProductSales = DB::table('sales_order_details as sod')
        ->select([
          'products.id as product_id',
          'products.name',
          DB::raw('SUM(sod.qty) as total_qty'),
          DB::raw('SUM(products.base_price * sod.qty) as total_base_price'),
          DB::raw('SUM(sod.price * sod.qty) as total_sell_price'),
          DB::raw('SUM((sod.price * sod.qty) - (products.base_price * sod.qty)) as total_profit'),
          DB::raw('(SUM((sod.price * sod.qty) - (products.base_price * sod.qty))
          / NULLIF(SUM(products.base_price * sod.qty), 0)) * 100 as total_profit_percentage'),
        ])
        ->join('products', 'sod.product_id', '=', 'products.id')
        ->join('sales_orders as so', 'sod.sales_order_id', '=', 'so.id')
        ->groupBy('products.id', 'products.name')
        ->orderBy('total_profit', 'desc')
        ->where(function ($query) use ($request) {
          $query->where('products.name', 'like', '%' . $request->q . '%');
        })
        ->where('so.status', '=', 'PAID');

      if (!is_null($request->start_date) && !is_null($request->end_date)) {
        $reportProductSales = $reportProductSales->whereBetween('so.created_at', [$request->start_date, $request->end_date]);
      }
      $reportProductSales = $reportProductSales->get()->map(function ($item) {
        $item->total_qty = (float) $item->total_qty;
        $item->total_base_price = (float) $item->total_base_price;
        $item->total_sell_price = (float) $item->total_sell_price;
        $item->total_profit = (float) $item->total_profit;
        $item->total_profit_percentage = (float) $item->total_profit_percentage;
        return $item;
      });


      return response()->json([
        'data' => $reportProductSales,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function reportProductStock(Request $request)
  {
    try {
      $reportProductStock = ProductStock::query()->with(['product']);

      if (!is_null($request->product_id)) {
        $reportProductStock->whereHas('product', function ($query) use ($request) {
          return $query->where('id', '=', $request->product_id);
        });
      }

      if (!is_null($request->start_date) && !is_null($request->end_date)) {
        $reportProductStock->whereBetween('created_at', [$request->start_date, $request->end_date]);
      }

      $result = $reportProductStock->latest('id')->get();

      return response()->json([
        'data' => $result,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }
  public function reportJournal(Request $request)
  {
    try {

      $reportJournal = Journal::query()->where(function ($query) use ($request) {
        $query->where('journal_number', 'like', '%' . $request->q . '%')
          ->orWhere('ref_number', 'like', '%' . $request->q . '%')
          ->orWhere('description', 'like', '%' . $request->q . '%');
      });
      if (!is_null($request->start_date) && !is_null($request->end_date)) {
        $reportJournal = $reportJournal->whereBetween('created_at', [$request->start_date, $request->end_date]);
      }

      $reportJournal = $reportJournal->latest('created_at')->get();

      return response()->json([
        'data' => $reportJournal,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function listProduct(Request $request)
  {
    try {
      $products = Product::query();

      if (!is_null($request->name)) {
        $products->where('name', 'like', '%' . $request->name . '%');
      }

      $result = $products->paginate(10);

      return response()->json([
        'data' => $result,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }
}
