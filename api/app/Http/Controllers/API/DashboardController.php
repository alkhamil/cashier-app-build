<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Product;
use App\Models\SalesOrder;
use App\Models\Supplier;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
  public function index(Request $request)
  {
    try {
      $totalSalesPaid = SalesOrder::where("status", "PAID");
      if (!is_null($request->start_date) && !is_null($request->end_date)) {
        $totalSalesPaid = $totalSalesPaid->whereBetween('created_at', [$request->start_date, $request->end_date]);
      }
      $totalSalesPaid = $totalSalesPaid->count();

      $totalSalesUnpaid = SalesOrder::where("status", "UNPAID");
      if (!is_null($request->start_date) && !is_null($request->end_date)) {
        $totalSalesUnpaid = $totalSalesUnpaid->whereBetween('created_at', [$request->start_date, $request->end_date]);
      }
      $totalSalesUnpaid = $totalSalesUnpaid->count();

      $totalSalesPending = SalesOrder::where("status", "DRAFT");
      if (!is_null($request->start_date) && !is_null($request->end_date)) {
        $totalSalesPending = $totalSalesPending->whereBetween('created_at', [$request->start_date, $request->end_date]);
      }
      $totalSalesPending = $totalSalesPending->count();

      $totalProductActive = Product::where("is_active", "1")->count();
      $totalCustomerActive = Customer::where("is_active", "1")->count();
      $totalSupplierActive = Supplier::where("is_active", "1")->count();

      $salesCalculation = DB::table(DB::raw('(
        SELECT
            so.created_at,
            so.sales_order_number,
            so.payment_method,
            so.sales_mode,
            c.name AS customer_name,
            so.status,
            COUNT(sod.id) AS total_item,
            SUM(p.base_price * sod.qty) AS total_base_price,
            SUM(sod.price * sod.qty) AS total_sell_price,
            SUM(sod.total_price - (p.base_price * sod.qty)) AS total_profit,
            (SUM(sod.total_price - (p.base_price * sod.qty)) /
            SUM(p.base_price * sod.qty)) * 100 AS total_profit_percentage
        FROM sales_orders AS so
        JOIN sales_order_details AS sod ON so.id = sod.sales_order_id
        JOIN products AS p ON sod.product_id = p.id
        JOIN customers AS c ON so.customer_id = c.id
        WHERE so.status = "PAID"
        GROUP BY
            so.created_at,
            so.sales_order_number,
            so.payment_method,
            so.sales_mode,
            c.name,
            so.status
    ) AS subquery'))
        ->selectRaw('SUM(total_base_price) AS sum_total_base_price')
        ->selectRaw('SUM(total_sell_price) AS sum_total_sell_price')
        ->selectRaw('SUM(total_profit) AS sum_total_profit')
        ->selectRaw('SUM(total_profit_percentage) AS sum_total_profit_percentage');

      if (!is_null($request->start_date) && !is_null($request->end_date)) {
        $salesCalculation = $salesCalculation->whereBetween('created_at', [$request->start_date, $request->end_date]);
      }

      $salesCalculation = $salesCalculation->get()
        ->map(function ($item) {
          $item->sum_total_base_price = (float) $item->sum_total_base_price;
          $item->sum_total_sell_price = (float) $item->sum_total_sell_price;
          $item->sum_total_profit = (float) $item->sum_total_profit;
          $item->sum_total_profit_percentage = (float) $item->sum_total_profit_percentage;
          return $item;
        });



      return response()->json([
        'data' => [
          'totalSalesPaid' => $totalSalesPaid,
          'totalSalesUnpaid' => $totalSalesUnpaid,
          'totalSalesPending' => $totalSalesPending,
          'totalSalesBase' => $salesCalculation[0]->sum_total_base_price,
          'totalSalesGross' => $salesCalculation[0]->sum_total_sell_price,
          'totalSalesLaba' => $salesCalculation[0]->sum_total_profit,
          'totalSalesLabaPercentage' => $salesCalculation[0]->sum_total_profit_percentage,
          'totalProductActive' => $totalProductActive,
          'totalCustomerActive' => $totalCustomerActive,
          'totalSupplierActive' => $totalSupplierActive,
        ],
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }
}
