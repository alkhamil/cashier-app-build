<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Journal;
use App\Models\Payable;
use App\Models\PayableDetail;
use App\Models\PurchaseOrder;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PayableController extends Controller
{
  public function index(Request $request)
  {
    try {

      $payables = Payable::query()->with([
        'purchase_order',
        'supplier',
      ]);

      $payables = $payables->where(function ($query) use ($request) {
        $query->where('payable_number', 'like', '%' . $request->q . '%')
          ->orWhereHas('purchase_order', function ($query) use ($request) {
            $query->where('purchase_order_number', 'like', '%' . $request->q . '%');
          })
          ->orWhereHas('supplier', function ($query) use ($request) {
            $query->where('name', 'like', '%' . $request->q . '%');
          });
      })->where('status', '=', 'UNPAID');

      if (!is_null($request->created_at)) {
        $created_at = date('Y-m-d', strtotime($request->created_at));
        $payables->whereDate('created_at', '=', $created_at);
      }

      if ($request->sort) {
        $order_type = 'asc';
        $order_column = $request->sort;
        if (str_contains($request->sort, '-')) {
          $order_type = 'desc';
          $order_column = substr($request->sort, 1);
        }

        $payables->orderBy($order_column, $order_type);
      }

      $result = $payables->latest('created_at')->get();

      return response()->json([
        'data' => $result,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function view(Request $request)
  {
    try {
      $payable = Payable::with([
        'purchase_order',
        'supplier',
      ])->firstWhere('id', $request->id);

      if (!$payable) {
        throw new Exception("Data not found!", 400);
      }

      return response()->json([
        'data' => $payable,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  private function saveJournal($item)
  {
    $journal = new Journal();
    $journal->journal_number = "JRN" . time();
    $journal->ref_number = $item["ref_number"];
    $journal->description = $item["description"];
    $journal->credit = $item["credit"];
    $journal->debit = $item["debit"];

    $latestJournal = DB::table('journals')
      ->latest('id')
      ->first();

    if ($latestJournal) {
      $journal->balance = $latestJournal->balance - $journal->debit;
    } else {
      $journal->balance = 0 - $journal->debit;
    }

    if (!$journal->save()) {
      throw new Exception('Failed transaction DB!', 500);
    }
  }

  public function update(Request $request)
  {
    DB::beginTransaction();
    try {
      $validator = Validator::make($request->all(), [
        'purchase_order_id' => 'required',
        'total_price' => 'required',
        'cash_amount' => 'required',
        'change_amount' => 'required',
        'amount' => 'required',
      ]);

      if ($validator->fails()) {
        throw new Exception($validator->errors(), 400);
      }

      $payable = Payable::firstWhere('id', $request->id);

      if (!$payable) {
        throw new Exception("Data not found!", 400);
      }

      $payable->amount = $request->change_amount;

      if ($payable->amount > 0) {
        $payable->status = "UNPAID";
      } else {
        $payable->status = "PAID";
      }

      $payable->updated_at = date('Y-m-d H:i:s');

      if (!$payable->save()) {
        throw new Exception('Failed transaction DB!', 500);
      }

      $payable_detail = new PayableDetail();
      $payable_detail->payable_id = $payable->id;
      $payable_detail->cash_amount = $request->amount;
      $payable->created_at = date('Y-m-d H:i:s');


      if (!$payable_detail->save()) {
        throw new Exception('Failed transaction DB!', 500);
      }

      $purchase_order = PurchaseOrder::firstWhere('id', $payable->purchase_order_id);
      if (!$purchase_order) {
        throw new Exception("Purchase Order not found!", 400);
      }

      $purchase_order->cash_amount = $request->cash_amount;
      $purchase_order->change_amount = $request->change_amount;

      if ($purchase_order->cash_amount >= $purchase_order->total_price) {
        $purchase_order->status = "PAID";
      } elseif ($purchase_order->cash_amount < $purchase_order->total_price && $purchase_order->change_amount < 0) {
        $purchase_order->status = "UNPAID";
      }

      if (!$purchase_order->save()) {
        throw new Exception('Failed transaction DB!', 500);
      }

      // @note: will be implement soon!
      // if ($purchase_order->status !== "DRAFT") {
      //   $this->saveJournal([
      //     "ref_number" => $purchase_order->purchase_order_number,
      //     "description" => "Pembelian",
      //     "credit" => 0,
      //     "debit" => $purchase_order->cash_amount
      //   ]);
      // }

      DB::commit();
      return response()->json([
        'data' => $payable,
        'message' => 'Successfuly Updated!'
      ], 200);
    } catch (Exception $error) {
      DB::rollBack();
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }
}
