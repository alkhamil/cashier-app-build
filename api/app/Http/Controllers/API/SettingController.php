<?php

namespace App\Http\Controllers\API;

use App\Helpers\PrinterSetting;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Mike42\Escpos\Printer;

class SettingController extends Controller
{
  public function view(Request $request)
  {
    try {
      $settings = Setting::pluck("value", "key")->toArray();


      if (!$settings) {
        throw new Exception("Data not found!", 400);
      }

      return response()->json([
        'data' => $settings,
        'message' => 'Successfuly Fetching'
      ], 200);
    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function update(Request $request)
  {
    DB::beginTransaction();
    try {
      $validator = Validator::make($request->all(), [
        'settings' => 'required|array',
      ]);


      if ($validator->fails()) {
        throw new Exception($validator->errors(), 400);
      }

      if (!empty($request->settings) && is_array($request->settings)) {
        foreach ($request->settings as $setting) {
          $_setting = Setting::where("key", $setting["key"])->first();
          if (!$_setting) {
            throw new Exception("Setting Key not found!", 400);
          }
          
          $_setting->value = $setting["value"];
          
          if (!$_setting->save()) {
            throw new Exception('Failed transaction DB!', 500);
          }
        }
      }

      DB::commit();
      return response()->json([
        'data' => null,
        'message' => 'Successfuly Created!'
      ], 201);
    } catch (Exception $error) {
      DB::rollBack();
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }

  public function testPrint(Request $request)
  {
    try {
      $settings = Setting::pluck("value", "key")->toArray();
      $printer = new PrinterSetting($settings);
      $printer->init();
      if ($printer) {
        $printer->setJustification(Printer::JUSTIFY_CENTER);
        $printer->text($settings["OUTLET_NAME"] . "\n");
        $printer->text($settings["OUTLET_ADDRESS"] . "\n");
        $printer->text($settings["OUTLET_PHONE"] . "\n");

        $printer->cut();
        $printer->close();
      } else {
        throw new Exception('Printer Not Connected!', 500);
      }

      return response()->json([
        "data"=> null,
        'message' => "Printer Connected"
      ], 200);

    } catch (Exception $error) {
      return response()->json([
        'message' => $error->getMessage()
      ], 500);
    }
  }
}
